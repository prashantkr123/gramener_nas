from django.shortcuts import render
from django.middleware.csrf import rotate_token
from django.http import HttpResponse


def home(request):
    return render(request, 'proxy/index.html')


def ques1(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques1.html')
    return HttpResponse('')


def ques1a_Maths(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques1a_Maths.html')
    return HttpResponse('')


def ques1a_Reading(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques1a_Reading.html')
    return HttpResponse('')


def ques1a_Science(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques1a_Science.html')
    return HttpResponse('')


def ques1a_Social(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques1a_Social.html')
    return HttpResponse('')


def ques2(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques2.html')
    return HttpResponse('')


def ques3(request):
    if request.method == 'GET':
        rotate_token(request)
        return render(request, 'proxy/ques3.html')
    return HttpResponse('')
