import math
import pandas as pd
import matplotlib.pyplot as plt
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_fatheroccup0 = 0
count_fatheroccup1 = 0
count_fatheroccup2 = 0
count_fatheroccup3 = 0
count_fatheroccup4 = 0
count_fatheroccup5 = 0
count_fatheroccup6 = 0
count_fatheroccup7 = 0
count_fatheroccup8 = 0
marks_fatheroccup0 = 0.00
marks_fatheroccup1 = 0.00
marks_fatheroccup2 = 0.00
marks_fatheroccup3 = 0.00
marks_fatheroccup4 = 0.00
marks_fatheroccup5 = 0.00
marks_fatheroccup6 = 0.00
marks_fatheroccup7 = 0.00
marks_fatheroccup8 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Father occupation").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Father occupation')
ax1.set_ylabel('Social %')
ax1.set_title("Father occupation vs Social score")
df1.plot(kind='bar')
fig.savefig('fatheroccup.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Father occupation'] == 0:
            count_fatheroccup0 = count_fatheroccup0 + 1
            marks_fatheroccup0 = marks_fatheroccup0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 1:
            count_fatheroccup1 = count_fatheroccup1 + 1
            marks_fatheroccup1 = marks_fatheroccup1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 2:
            count_fatheroccup2 = count_fatheroccup2 + 1
            marks_fatheroccup2 = marks_fatheroccup2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 3:
            count_fatheroccup3 = count_fatheroccup3 + 1
            marks_fatheroccup3 = marks_fatheroccup3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 4:
            count_fatheroccup4 = count_fatheroccup4 + 1
            marks_fatheroccup4 = marks_fatheroccup4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 5:
            count_fatheroccup5 = count_fatheroccup5 + 1
            marks_fatheroccup5 = marks_fatheroccup5 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 6:
            count_fatheroccup6 = count_fatheroccup6 + 1
            marks_fatheroccup6 = marks_fatheroccup6 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 7:
            count_fatheroccup7 = count_fatheroccup7 + 1
            marks_fatheroccup7 = marks_fatheroccup7 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father occupation'] == 8:
            count_fatheroccup8 = count_fatheroccup8 + 1
            marks_fatheroccup8 = marks_fatheroccup8 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Father occupation(Don\'t know) = ',
      (marks_fatheroccup0 / count_fatheroccup0))
print('Average % of marks for Father occupation(Unemployed) = ',
      (marks_fatheroccup1 / count_fatheroccup1))
print('Average % of marks for Father occupation(Labourer) = ',
      (marks_fatheroccup2 / count_fatheroccup2))
print('Average % of marks for Father occupation(Farmer) = ',
      (marks_fatheroccup3 / count_fatheroccup3))
print('Average % of marks for Father occupation(Clerk) = ',
      (marks_fatheroccup4 / count_fatheroccup4))
print('Average % of marks for Father occupation(Skilled worker) = ',
      (marks_fatheroccup5 / count_fatheroccup5))
print('Average % of marks for Father occupation(Business) = ',
      (marks_fatheroccup6 / count_fatheroccup6))
print('Average % of marks for Father occupation(Teacher / Lecturer) = ',
      (marks_fatheroccup7 / count_fatheroccup7))
print('Average % of marks for Father occupation(professional) = ',
      (marks_fatheroccup8 / count_fatheroccup8)) '''
