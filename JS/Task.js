function splicingElements(arr, element) {
    var i = arr.length
    while (i--) {
        if (arr[i] === element)
            arr.splice(i, 1)
    }
    return arr
}

var data = [0, 1, 2, 'stop', 2, 0, 1, 'stop', 0]
console.log(splicingElements(data, 0))