import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_handicap0 = 0
count_handicap1 = 0
count_handicap2 = 0
marks_handicap0 = 0.00
marks_handicap1 = 0.00
marks_handicap2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Handicap'] == 0:
            count_handicap0 = count_handicap0 + 1
            marks_handicap0 = marks_handicap0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Handicap'] == 1:
            count_handicap1 = count_handicap1 + 1
            marks_handicap1 = marks_handicap1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Handicap'] == 2:
            count_handicap2 = count_handicap2 + 1
            marks_handicap2 = marks_handicap2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Handicap(Unknown) = ',
      (marks_handicap0 / count_handicap0))
print('Average % of marks for Handicap(Yes) = ',
      (marks_handicap1 / count_handicap1))
print('Average % of marks for Handicap(No) = ',
      (marks_handicap2 / count_handicap2))
