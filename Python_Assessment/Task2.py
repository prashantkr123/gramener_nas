''' Since 1-1-1990 falls on Monday and 31-12-2000 falls on Monday itself
    therefore, what we get quotient after dividing the total numbe
    r of days by 7 will be the total number of thursday and if the
    remainder is found to be 3 then the number
    of thursdays will incremented by 1. '''

from datetime import date
from datetime import timedelta
date1 = date(1990, 1, 1)
date2 = date(2000, 12, 31)
days = (date1 - date2)
print((days + timedelta(-1)) / 7)
