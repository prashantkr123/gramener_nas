import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_computeruse1 = 0
count_computeruse2 = 0
count_computeruse3 = 0
count_computeruse4 = 0
count_computeruse5 = 0
marks_computeruse1 = 0.00
marks_computeruse2 = 0.00
marks_computeruse3 = 0.00
marks_computeruse4 = 0.00
marks_computeruse5 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Computer use'] == 1:
            count_computeruse1 = count_computeruse1 + 1
            marks_computeruse1 = marks_computeruse1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Computer use'] == 2:
            count_computeruse2 = count_computeruse2 + 1
            marks_computeruse2 = marks_computeruse2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Computer use'] == 3:
            count_computeruse3 = count_computeruse3 + 1
            marks_computeruse3 = marks_computeruse3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Computer use'] == 4:
            count_computeruse4 = count_computeruse4 + 1
            marks_computeruse4 = marks_computeruse4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Computer use'] == 5:
            count_computeruse5 = count_computeruse5 + 1
            marks_computeruse5 = marks_computeruse5 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Category(No computer) = ',
      (marks_computeruse1 / count_computeruse1))
print('Average % of marks for Category(Never use) = ',
      (marks_computeruse2 / count_computeruse2))
print('Average % of marks for Category(once in a month) = ',
      (marks_computeruse3 / count_computeruse3))
print('Average % of marks for Category(once in a week) = ',
      (marks_computeruse4 / count_computeruse4))
print('Average % of marks for Category(Daily) = ',
      (marks_computeruse5 / count_computeruse5))
