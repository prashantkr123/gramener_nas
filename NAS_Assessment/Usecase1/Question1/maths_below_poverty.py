import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_bpl0 = 0
count_bpl1 = 0
count_bpl2 = 0
marks_bpl0 = 0.00
marks_bpl1 = 0.00
marks_bpl2 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Below poverty").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Below poverty')
ax1.set_ylabel('Social %')
ax1.set_title("Below poverty vs Social score")
df1.plot(kind='bar')
fig.savefig('bpl.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Below poverty'] == 0:
            count_bpl0 = count_bpl0 + 1
            marks_bpl0 = marks_bpl0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Below poverty'] == 1:
            count_bpl1 = count_bpl1 + 1
            marks_bpl1 = marks_bpl1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Below poverty'] == 2:
            count_bpl2 = count_bpl2 + 1
            marks_bpl2 = marks_bpl2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Below poverty(Don\'t know) = ',
      (marks_bpl0 / count_bpl0))
print('Average % of marks for Below poverty(No) = ',
      (marks_bpl1 / count_bpl1))
print('Average % of marks for Below poverty(Yes) = ',
      (marks_bpl2 / count_bpl2)) '''
