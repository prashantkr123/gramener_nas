import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_boy = 0
count_girl = 0
marks_boy = 0.00
marks_girl = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Gender'] == 1:
            count_boy = count_boy + 1
            marks_boy = marks_boy + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Gender'] == 2:
            count_girl = count_girl + 1
            marks_girl = marks_girl + dataframe.iloc[i]['Maths %']
print('Average % of marks for Gender(Boy) = ',
      (marks_boy / count_boy))
print('Average % of marks for Gender(Girl) = ',
      (marks_girl / count_girl))
