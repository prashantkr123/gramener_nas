import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_likeschool1 = 0
count_likeschool2 = 0
marks_likeschool1 = 0.00
marks_likeschool2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Like school'] == 1:
            count_likeschool1 = count_likeschool1 + 1
            marks_likeschool1 = marks_likeschool1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Like school'] == 2:
            count_likeschool2 = count_likeschool2 + 1
            marks_likeschool2 = marks_likeschool2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Like school(No) = ',
      (marks_likeschool1 / count_likeschool1))
print('Average % of marks for Like school(Yes) = ',
      (marks_likeschool2 / count_likeschool2))
