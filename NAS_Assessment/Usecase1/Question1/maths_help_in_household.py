import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_helphousehold1 = 0
count_helphousehold2 = 0
count_helphousehold3 = 0
count_helphousehold4 = 0
marks_helphousehold1 = 0.00
marks_helphousehold2 = 0.00
marks_helphousehold3 = 0.00
marks_helphousehold4 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Help in household'] == 1:
            count_helphousehold1 = count_helphousehold1 + 1
            marks_helphousehold1 = marks_helphousehold1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Help in household'] == 2:
            count_helphousehold2 = count_helphousehold2 + 1
            marks_helphousehold2 = marks_helphousehold2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Help in household'] == 3:
            count_helphousehold3 = count_helphousehold3 + 1
            marks_helphousehold3 = marks_helphousehold3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Help in household'] == 4:
            count_helphousehold4 = count_helphousehold4 + 1
            marks_helphousehold4 = marks_helphousehold4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Help in household(Never) = ',
      (marks_helphousehold1 / count_helphousehold1))
print('Average % of marks for Help in household(Once a month) = ',
      (marks_helphousehold2 / count_helphousehold2))
print('Average % of marks for Help in household(Once a week) = ',
      (marks_helphousehold3 / count_helphousehold3))
print('Average % of marks for Help in household(Every day) = ',
      (marks_helphousehold4 / count_helphousehold4))
