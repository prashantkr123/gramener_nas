import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_category0 = 0
count_category1 = 0
count_category2 = 0
count_category3 = 0
count_category4 = 0
marks_category0 = 0.00
marks_category1 = 0.00
marks_category2 = 0.00
marks_category3 = 0.00
marks_category4 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Category").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Category')
ax1.set_ylabel('Social %')
ax1.set_title("Category vs Social score")
df1.plot(kind='bar')
fig.savefig('category.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Category'] == 0:
            count_category0 = count_category0 + 1
            marks_category0 = marks_category0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Category'] == 1:
            count_category1 = count_category1 + 1
            marks_category1 = marks_category1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Category'] == 2:
            count_category2 = count_category2 + 1
            marks_category2 = marks_category2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Category'] == 3:
            count_category3 = count_category3 + 1
            marks_category3 = marks_category3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Category'] == 4:
            count_category4 = count_category4 + 1
            marks_category4 = marks_category4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Category(0) = ',
      (marks_category0 / count_category0))
print('Average % of marks for Category(1) = ',
      (marks_category1 / count_category1))
print('Average % of marks for Category(2) = ',
      (marks_category2 / count_category2))
print('Average % of marks for Category(3) = ',
      (marks_category3 / count_category3))
print('Average % of marks for Category(4) = ',
      (marks_category4 / count_category4)) '''
