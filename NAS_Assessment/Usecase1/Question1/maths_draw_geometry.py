import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_drawgeometry1 = 0
count_drawgeometry2 = 0
count_drawgeometry3 = 0
marks_drawgeometry1 = 0.00
marks_drawgeometry2 = 0.00
marks_drawgeometry3 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Draw geometry").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Draw geometry')
ax1.set_ylabel('Social %')
ax1.set_title("Draw geometry vs Social score")
df1.plot(kind='bar')
fig.savefig('drawgeometry.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Draw geometry'] == 1:
            count_drawgeometry1 = count_drawgeometry1 + 1
            marks_drawgeometry1 = marks_drawgeometry1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Draw geometry'] == 2:
            count_drawgeometry2 = count_drawgeometry2 + 1
            marks_drawgeometry2 = marks_drawgeometry2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Draw geometry'] == 3:
            count_drawgeometry3 = count_drawgeometry3 + 1
            marks_drawgeometry3 = marks_drawgeometry3 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Draw geometry(Disagree) = ',
      (marks_drawgeometry1 / count_drawgeometry1))
print('Average % of marks for Draw geometry(neither) = ',
      (marks_drawgeometry2 / count_drawgeometry2))
print('Average % of marks for Draw  geometry(agree) = ',
      (marks_drawgeometry3 / count_drawgeometry3)) '''
