import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_playgames1 = 0
count_playgames2 = 0
count_playgames3 = 0
count_playgames4 = 0
marks_playgames1 = 0.00
marks_playgames2 = 0.00
marks_playgames3 = 0.00
marks_playgames4 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Play games'] == 1:
            count_playgames1 = count_playgames1 + 1
            marks_playgames1 = marks_playgames1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Play games'] == 2:
            count_playgames2 = count_playgames2 + 1
            marks_playgames2 = marks_playgames2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Play games'] == 3:
            count_playgames3 = count_playgames3 + 1
            marks_playgames3 = marks_playgames3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Play games'] == 4:
            count_playgames4 = count_playgames4 + 1
            marks_playgames4 = marks_playgames4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Play games(Never) = ',
      (marks_playgames1 / count_playgames1))
print('Average % of marks for Play games(Once a month) = ',
      (marks_playgames2 / count_playgames2))
print('Average % of marks for Play games(Once a week) = ',
      (marks_playgames3 / count_playgames3))
print('Average % of marks for Play games(Every day) = ',
      (marks_playgames4 / count_playgames4))
