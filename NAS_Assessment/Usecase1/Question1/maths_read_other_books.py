import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_readotherbooks1 = 0
count_readotherbooks2 = 0
marks_readotherbooks1 = 0.00
marks_readotherbooks2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Read other books'] == 1:
            count_readotherbooks1 = count_readotherbooks1 + 1
            marks_readotherbooks1 = marks_readotherbooks1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Read other books'] == 2:
            count_readotherbooks2 = count_readotherbooks2 + 1
            marks_readotherbooks2 = marks_readotherbooks2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Read ohter books(No) = ',
      (marks_readotherbooks1 / count_readotherbooks1))
print('Average % of marks for Read other books(Yes) = ',
      (marks_readotherbooks2 / count_readotherbooks2))
