import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_usedictionary1 = 0
count_usedictionary2 = 0
marks_usedictionary1 = 0.00
marks_usedictionary2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Use dictionary'] == 1:
            count_usedictionary1 = count_usedictionary1 + 1
            marks_usedictionary1 = marks_usedictionary1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Use dictionary'] == 2:
            count_usedictionary2 = count_usedictionary2 + 1
            marks_usedictionary2 = marks_usedictionary2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Use dictionary(No) = ',
      (marks_usedictionary1 / count_usedictionary1))
print('Average % of marks for Use dictionary(Yes) = ',
      (marks_usedictionary2 / count_usedictionary2))
