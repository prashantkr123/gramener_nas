import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_distance1 = 0
count_distance2 = 0
count_distance3 = 0
count_distance4 = 0
marks_distance1 = 0.00
marks_distance2 = 0.00
marks_distance3 = 0.00
marks_distance4 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Distance").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Distance')
ax1.set_ylabel('Social %')
ax1.set_title("Distance vs Social score")
df1.plot(kind='bar')
fig.savefig('distance.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Distance'] == 1:
            count_distance1 = count_distance1 + 1
            marks_distance1 = marks_distance1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Distance'] == 2:
            count_distance2 = count_distance2 + 1
            marks_distance2 = marks_distance2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Distance'] == 3:
            count_distance3 = count_distance3 + 1
            marks_distance3 = marks_distance3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Distance'] == 4:
            count_distance4 = count_distance4 + 1
            marks_distance4 = marks_distance4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Distance(upto 1 km) = ',
      (marks_distance1 / count_distance1))
print('Average % of marks for Distance(1-3 km) = ',
      (marks_distance2 / count_distance2))
print('Average % of marks for Distance(3-5 km) = ',
      (marks_distance3 / count_distance3))
print('Average % of marks for Distance(more than 5 km) = ',
      (marks_distance4 / count_distance4))'''
