import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_motheredu0 = 0
count_motheredu1 = 0
count_motheredu2 = 0
count_motheredu3 = 0
count_motheredu4 = 0
count_motheredu5 = 0
marks_motheredu0 = 0.00
marks_motheredu1 = 0.00
marks_motheredu2 = 0.00
marks_motheredu3 = 0.00
marks_motheredu4 = 0.00
marks_motheredu5 = 0.00

for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Mother edu'] == 0:
            count_motheredu0 = count_motheredu0 + 1
            marks_motheredu0 = marks_motheredu0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother edu'] == 1:
            count_motheredu1 = count_motheredu1 + 1
            marks_motheredu1 = marks_motheredu1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother edu'] == 2:
            count_motheredu2 = count_motheredu2 + 1
            marks_motheredu2 = marks_motheredu2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother edu'] == 3:
            count_motheredu3 = count_motheredu3 + 1
            marks_motheredu3 = marks_motheredu3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother edu'] == 4:
            count_motheredu4 = count_motheredu4 + 1
            marks_motheredu4 = marks_motheredu4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother edu'] == 5:
            count_motheredu5 = count_motheredu5 + 1
            marks_motheredu5 = marks_motheredu5 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Mother edu(NA) = ',
      (marks_motheredu0 / count_motheredu0))
print('Average % of marks for Mother edu(Illiterate) = ',
      (marks_motheredu1 / count_motheredu1))
print('Average % of marks for Mother edu(Primary) = ',
      (marks_motheredu2 / count_motheredu2))
print('Average % of marks for Mother edu(Secondary) = ',
      (marks_motheredu3 / count_motheredu3))
print('Average % of marks for Mother edu(Sr Secondary) = ',
      (marks_motheredu4 / count_motheredu4))
print('Average % of marks for Mother edu(Degree & above) = ',
      (marks_motheredu5 / count_motheredu5))
