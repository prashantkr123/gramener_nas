import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_maths_all = 0
marks_maths_all = 0.00
count_sc_all = 0
marks_sc_all = 0.00
count_maths_south = 0
marks_maths_south = 0.00
count_sc_south = 0
marks_sc_south = 0.00
south_states = ['AP', 'AN', 'KA', 'KL', 'PY', 'TN']
states_all = ['AN', 'AP', 'AR', 'BR', 'CG', 'CH', 'DD', 'DL', 'DN',
              'GA', 'GJ', 'HP', 'HR', 'JH', 'JK', 'KA', 'KL', 'MG', 'MH', 'MN',
              'MP', 'MZ', 'NG', 'OR', 'PB', 'PY', 'RJ', 'SK', 'TN',
              'TR', 'UK', 'UP', 'WB']
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
            state_code = states_all.index(dataframe.iloc[i]['State'])
            count_maths_all = count_maths_all + 1
            marks_maths_all = marks_maths_all + dataframe.iloc[i]['Maths %']
            if dataframe.iloc[i]['State'] in south_states:
                marks_maths_south = marks_maths_south + dataframe.iloc[i]['Maths %']
                count_maths_south = count_maths_south + 1
    if (math.isnan(dataframe.iloc[i]['Science %'])) is False:
            state_code = states_all.index(dataframe.iloc[i]['State'])
            count_sc_all = count_sc_all + 1
            marks_sc_all = marks_sc_all + dataframe.iloc[i]['Science %']
            if dataframe.iloc[i]['State'] in south_states:
                marks_sc_south = marks_sc_south+ dataframe.iloc[i]['Science %']
                count_sc_south = count_sc_south + 1
print('National average for maths % is ', (marks_maths_all / count_maths_all))
print('National average for science % is', (marks_sc_all / count_sc_all))
print('South average for maths % is', (marks_maths_south / count_maths_south))
print('South average for science % is', (marks_sc_south / count_sc_south))
