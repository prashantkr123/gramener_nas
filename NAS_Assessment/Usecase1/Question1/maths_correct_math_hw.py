import math
import pandas as pd
import matplotlib.pyplot as plt
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_correctmathhw1 = 0
count_correctmathhw2 = 0
count_correctmathhw3 = 0
count_correctmathhw4 = 0
marks_correctmathhw1 = 0.00
marks_correctmathhw2 = 0.00
marks_correctmathhw3 = 0.00
marks_correctmathhw4 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Correct Math HW").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Correct Math HW')
ax1.set_ylabel('Social %')
ax1.set_title("Correct Math HW vs Social score")
df1.plot(kind='bar')
fig.savefig('correctmathhw.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Correct Math HW'] == 1:
            count_correctmathhw1 = count_correctmathhw1 + 1
            marks_correctmathhw1 = marks_correctmathhw1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Correct Math HW'] == 2:
            count_correctmathhw2 = count_correctmathhw2 + 1
            marks_correctmathhw2 = marks_correctmathhw2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Correct Math HW'] == 3:
            count_correctmathhw3 = count_correctmathhw3 + 1
            marks_correctmathhw3 = marks_correctmathhw3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Correct Math HW'] == 4:
            count_correctmathhw4 = count_correctmathhw4 + 1
            marks_correctmathhw4 = marks_correctmathhw4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Coorect Math HW(Never) = ',
      (marks_correctmathhw1 / count_correctmathhw1))
print('Average % of marks for Correct Math HW(1-2 times a week) = ',
      (marks_correctmathhw2 / count_correctmathhw2))
print('Average % of marks for Correct Math HW(3-4 times a week) = ',
      (marks_correctmathhw3 / count_correctmathhw3))
print('Average % of marks for Correct Math HW(Everyday) = ',
      (marks_correctmathhw4 / count_correctmathhw4))'''
