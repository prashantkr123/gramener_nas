"""frontend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from proxy import views as proxy_views

urlpatterns = [
    url(r'^$', proxy_views.home),
    url(r'^ques1/$', proxy_views.ques1),
    url(r'^ques1a_Maths/$', proxy_views.ques1a_Maths),
    url(r'^ques1a_Reading/$', proxy_views.ques1a_Reading),
    url(r'^ques1a_Science/$', proxy_views.ques1a_Science),
    url(r'^ques1a_Social/$', proxy_views.ques1a_Social),
    url(r'^ques2/$', proxy_views.ques2),
    url(r'^ques3/$', proxy_views.ques3)

]
