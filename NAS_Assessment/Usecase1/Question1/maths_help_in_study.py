import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_helpstudy1 = 0
count_helpstudy2 = 0
marks_helpstudy1 = 0.00
marks_helpstudy2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Help in Study'] == 1:
            count_helpstudy1 = count_helpstudy1 + 1
            marks_helpstudy1 = marks_helpstudy1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Help in Study'] == 2:
            count_helpstudy2 = count_helpstudy2 + 1
            marks_helpstudy2 = marks_helpstudy2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Help in Study(No) = ',
      (marks_helpstudy1 / count_helpstudy1))
print('Average % of marks for Help in Study(Yes) = ',
      (marks_helpstudy2 / count_helpstudy2))
