import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_mathsdifficult1 = 0
count_mathsdifficult2 = 0
count_mathsdifficult3 = 0
marks_mathsdifficult1 = 0.00
marks_mathsdifficult2 = 0.00
marks_mathsdifficult3 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Maths is difficult'] == 1:
            count_mathsdifficult1 = count_mathsdifficult1 + 1
            marks_mathsdifficult1 = marks_mathsdifficult1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Maths is difficult'] == 2:
            count_mathsdifficult2 = count_mathsdifficult2 + 1
            marks_mathsdifficult2 = marks_mathsdifficult2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Maths is difficult'] == 3:
            count_mathsdifficult3 = count_mathsdifficult3 + 1
            marks_mathsdifficult3 = marks_mathsdifficult3 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Maths is difficult(Disagree) = ',
      (marks_mathsdifficult1 / count_mathsdifficult1))
print('Average % of marks for Maths is difficult(Neither) = ',
      (marks_mathsdifficult2 / count_mathsdifficult2))
print('Average % of marks for Maths is difficult(Agree) = ',
      (marks_mathsdifficult3 / count_mathsdifficult3))
