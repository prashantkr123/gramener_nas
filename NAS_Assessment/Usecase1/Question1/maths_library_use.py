import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_libraryuse1 = 0
count_libraryuse2 = 0
count_libraryuse3 = 0
count_libraryuse4 = 0
count_libraryuse5 = 0
marks_libraryuse1 = 0.00
marks_libraryuse2 = 0.00
marks_libraryuse3 = 0.00
marks_libraryuse4 = 0.00
marks_libraryuse5 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Library use'] == 1:
            count_libraryuse1 = count_libraryuse1 + 1
            marks_libraryuse1 = marks_libraryuse1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Library use'] == 2:
            count_libraryuse2 = count_libraryuse2 + 1
            marks_libraryuse2 = marks_libraryuse2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Library use'] == 3:
            count_libraryuse3 = count_libraryuse3 + 1
            marks_libraryuse3 = marks_libraryuse3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Library use'] == 4:
            count_libraryuse4 = count_libraryuse4 + 1
            marks_libraryuse4 = marks_libraryuse4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Library use'] == 5:
            count_libraryuse5 = count_libraryuse5 + 1
            marks_libraryuse5 = marks_libraryuse5 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Library use(No library) = ',
      (marks_libraryuse1 / count_libraryuse1))
print('Average % of marks for Library use(Never use) = ',
      (marks_libraryuse2 / count_libraryuse2))
print('Average % of marks for Library use(once in a month) = ',
      (marks_libraryuse3 / count_libraryuse3))
print('Average % of marks for Library use(once in a week) = ',
      (marks_libraryuse4 / count_libraryuse4))
print('Average % of marks for Library use(more than once in a week) = ',
      (marks_libraryuse5 / count_libraryuse5))
