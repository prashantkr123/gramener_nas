import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_usecalculator1 = 0
count_usecalculator2 = 0
marks_usecalculator1 = 0.00
marks_usecalculator2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Use calculator'] == 1:
            count_usecalculator1 = count_usecalculator1 + 1
            marks_usecalculator1 = marks_usecalculator1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Use calculator'] == 2:
            count_usecalculator2 = count_usecalculator2 + 1
            marks_usecalculator2 = marks_usecalculator2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Use calculator(No) = ',
      (marks_usecalculator1 / count_usecalculator1))
print('Average % of marks for Use calculator(Yes) = ',
      (marks_usecalculator2 / count_usecalculator2))
