import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_fatheredu0 = 0
count_fatheredu1 = 0
count_fatheredu2 = 0
count_fatheredu3 = 0
count_fatheredu4 = 0
count_fatheredu5 = 0
marks_fatheredu0 = 0.00
marks_fatheredu1 = 0.00
marks_fatheredu2 = 0.00
marks_fatheredu3 = 0.00
marks_fatheredu4 = 0.00
marks_fatheredu5 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Father edu").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Father edu')
ax1.set_ylabel('Social %')
ax1.set_title("Father edu vs Social score")
df1.plot(kind='bar')
fig.savefig('fatheredu.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Father edu'] == 0:
            count_fatheredu0 = count_fatheredu0 + 1
            marks_fatheredu0 = marks_fatheredu0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father edu'] == 1:
            count_fatheredu1 = count_fatheredu1 + 1
            marks_fatheredu1 = marks_fatheredu1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father edu'] == 2:
            count_fatheredu2 = count_fatheredu2 + 1
            marks_fatheredu2 = marks_fatheredu2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father edu'] == 3:
            count_fatheredu3 = count_fatheredu3 + 1
            marks_fatheredu3 = marks_fatheredu3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father edu'] == 4:
            count_fatheredu4 = count_fatheredu4 + 1
            marks_fatheredu4 = marks_fatheredu4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Father edu'] == 5:
            count_fatheredu5 = count_fatheredu5 + 1
            marks_fatheredu5 = marks_fatheredu5 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Father edu(NA) = ',
      (marks_fatheredu0 / count_fatheredu0))
print('Average % of marks for Father edu(Illiterate) = ',
      (marks_fatheredu1 / count_fatheredu1))
print('Average % of marks for Father edu(Primary) = ',
      (marks_fatheredu2 / count_fatheredu2))
print('Average % of marks for Father edu(Secondary) = ',
      (marks_fatheredu3 / count_fatheredu3))
print('Average % of marks for Father edu(Sr Secondary) = ',
      (marks_fatheredu4 / count_fatheredu4))
print('Average % of marks for Father edu(Degree & above) = ',
      (marks_fatheredu5 / count_fatheredu5)) '''
