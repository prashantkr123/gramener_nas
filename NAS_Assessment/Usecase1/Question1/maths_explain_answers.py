import math
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_explainanswers1 = 0
count_explainanswers2 = 0
count_explainanswers3 = 0
marks_explainanswers1 = 0.00
marks_explainanswers2 = 0.00
marks_explainanswers3 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("Explain answers").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('Explain answers')
ax1.set_ylabel('Social %')
ax1.set_title("Explain answers vs Social score")
df1.plot(kind='bar')
fig.savefig('explainanswers.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Explain answers'] == 1:
            count_explainanswers1 = count_explainanswers1 + 1
            marks_explainanswers1 = marks_explainanswers1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Explain answers'] == 2:
            count_explainanswers2 = count_explainanswers2 + 1
            marks_explainanswers2 = marks_explainanswers2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Explain answers'] == 3:
            count_explainanswers3 = count_explainanswers3 + 1
            marks_explainanswers3 = marks_explainanswers3 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Explain answers(Disagree) = ',
      (marks_explainanswers1 / count_explainanswers1))
print('Average % of marks for Explain answers(Neither) = ',
      (marks_explainanswers2 / count_explainanswers2))
print('Average % of marks for Explain answers(Agree) = ',
      (marks_explainanswers3 / count_explainanswers3)) '''
