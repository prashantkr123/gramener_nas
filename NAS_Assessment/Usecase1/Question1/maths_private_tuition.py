import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_privatetuition1 = 0
count_privatetuition2 = 0
marks_privatetuition1 = 0.00
marks_privatetuition2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Private tuition'] == 1:
            count_privatetuition1 = count_privatetuition1 + 1
            marks_privatetuition1 = marks_privatetuition1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Private tuition'] == 2:
            count_privatetuition2 = count_privatetuition2 + 1
            marks_privatetuition2 = marks_privatetuition2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Private tuition(No) = ',
      (marks_privatetuition1 / count_privatetuition1))
print('Average % of marks for Private tuition(Yes) = ',
      (marks_privatetuition2 / count_privatetuition2))
