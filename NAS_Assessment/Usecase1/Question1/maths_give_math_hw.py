import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_givemathhw1 = 0
count_givemathhw2 = 0
count_givemathhw3 = 0
count_givemathhw4 = 0
marks_givemathhw1 = 0.00
marks_givemathhw2 = 0.00
marks_givemathhw3 = 0.00
marks_givemathhw4 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Give Math HW'] == 1:
            count_givemathhw1 = count_givemathhw1 + 1
            marks_givemathhw1 = marks_givemathhw1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Give Math HW'] == 2:
            count_givemathhw2 = count_givemathhw2 + 1
            marks_givemathhw2 = marks_givemathhw2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Give Math HW'] == 3:
            count_givemathhw3 = count_givemathhw3 + 1
            marks_givemathhw3 = marks_givemathhw3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Give Math HW'] == 4:
            count_givemathhw4 = count_givemathhw4 + 1
            marks_givemathhw4 = marks_givemathhw4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Give Math HW(Never) = ',
      (marks_givemathhw1 / count_givemathhw1))
print('Average % of marks for Give Math HW(1-2 times a week) = ',
      (marks_givemathhw2 / count_givemathhw2))
print('Average % of marks for Give Math HW(3-4 times a week) = ',
      (marks_givemathhw3 / count_givemathhw3))
print('Average % of marks for Give Math HW(Everyday) = ',
      (marks_givemathhw4 / count_givemathhw4))
