import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_motheroccup0 = 0
count_motheroccup1 = 0
count_motheroccup2 = 0
count_motheroccup3 = 0
count_motheroccup4 = 0
count_motheroccup5 = 0
count_motheroccup6 = 0
count_motheroccup7 = 0
count_motheroccup8 = 0
marks_motheroccup0 = 0.00
marks_motheroccup1 = 0.00
marks_motheroccup2 = 0.00
marks_motheroccup3 = 0.00
marks_motheroccup4 = 0.00
marks_motheroccup5 = 0.00
marks_motheroccup6 = 0.00
marks_motheroccup7 = 0.00
marks_motheroccup8 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Mother occupation'] == 0:
            count_motheroccup0 = count_motheroccup0 + 1
            marks_motheroccup0 = marks_motheroccup0 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 1:
            count_motheroccup1 = count_motheroccup1 + 1
            marks_motheroccup1 = marks_motheroccup1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 2:
            count_motheroccup2 = count_motheroccup2 + 1
            marks_motheroccup2 = marks_motheroccup2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 3:
            count_motheroccup3 = count_motheroccup3 + 1
            marks_motheroccup3 = marks_motheroccup3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 4:
            count_motheroccup4 = count_motheroccup4 + 1
            marks_motheroccup4 = marks_motheroccup4 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 5:
            count_motheroccup5 = count_motheroccup5 + 1
            marks_motheroccup5 = marks_motheroccup5 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 6:
            count_motheroccup6 = count_motheroccup6 + 1
            marks_motheroccup6 = marks_motheroccup6 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 7:
            count_motheroccup7 = count_motheroccup7 + 1
            marks_motheroccup7 = marks_motheroccup7 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Mother occupation'] == 8:
            count_motheroccup8 = count_motheroccup8 + 1
            marks_motheroccup8 = marks_motheroccup8 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Mother occupation(Don\'t know) = ',
      (marks_motheroccup0 / count_motheroccup0))
print('Average % of marks for Mother occupation(Unemployed) = ',
      (marks_motheroccup1 / count_motheroccup1))
print('Average % of marks for Mother occupation(Labourer) = ',
      (marks_motheroccup2 / count_motheroccup2))
print('Average % of marks for Mother occupation(Farmer) = ',
      (marks_motheroccup3 / count_motheroccup3))
print('Average % of marks for Mother occupation(Clerk) = ',
      (marks_motheroccup4 / count_motheroccup4))
print('Average % of marks for Mother occupation(Skilled worker) = ',
      (marks_motheroccup5 / count_motheroccup5))
print('Average % of marks for Mother occupation(Business) = ',
      (marks_motheroccup6 / count_motheroccup6))
print('Average % of marks for Mother occupation(Teacher / Lecturer) = ',
      (marks_motheroccup7 / count_motheroccup7))
print('Average % of marks for Mother occupation(professional) = ',
      (marks_motheroccup8 / count_motheroccup8))
