import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_useinternet1 = 0
count_useinternet2 = 0
marks_useinternet1 = 0.00
marks_useinternet2 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Use Internet'] == 1:
            count_useinternet1 = count_useinternet1 + 1
            marks_useinternet1 = marks_useinternet1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Use Internet'] == 2:
            count_useinternet2 = count_useinternet2 + 1
            marks_useinternet2 = marks_useinternet2 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Use Internet(No) = ',
      (marks_useinternet1 / count_useinternet1))
print('Average % of marks for Use Internet(Yes) = ',
      (marks_useinternet2 / count_useinternet2))
