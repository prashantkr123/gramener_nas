import math
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_solvemaths1 = 0
count_solvemaths2 = 0
count_solvemaths3 = 0
marks_solvemaths1 = 0.00
marks_solvemaths2 = 0.00
marks_solvemaths3 = 0.00
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['Solve Maths'] == 1:
            count_solvemaths1 = count_solvemaths1 + 1
            marks_solvemaths1 = marks_solvemaths1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Solve Maths'] == 2:
            count_solvemaths2 = count_solvemaths2 + 1
            marks_solvemaths2 = marks_solvemaths2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['Solve Maths'] == 3:
            count_solvemaths3 = count_solvemaths3 + 1
            marks_solvemaths3 = marks_solvemaths3 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Solve Maths(Disagree) = ',
      (marks_solvemaths1 / count_solvemaths1))
print('Average % of marks for Solve Maths(Neither) = ',
      (marks_solvemaths2 / count_solvemaths2))
print('Average % of marks for Solve Maths(Agree) = ',
      (marks_solvemaths3 / count_solvemaths3))
