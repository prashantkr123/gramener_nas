import math
import pandas as pd
import matplotlib.pyplot as plt
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_books1 = 0
count_books2 = 0
count_books3 = 0
count_books4 = 0
marks_books1 = 0.00
marks_books2 = 0.00
marks_books3 = 0.00
marks_books4 = 0.00
df = dataframe.rename(columns={'Social %': 'Social'})
df1 = df.groupby("# Books").Social.mean()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlabel('# Books')
ax1.set_ylabel('Social %')
ax1.set_title("Books vs Social score")
df1.plot(kind='bar')
fig.savefig('books.png')
'''for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
        if dataframe.iloc[i]['# Books'] == 1:
            count_books1 = count_books1 + 1
            marks_books1 = marks_books1 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['# Books'] == 2:
            count_books2 = count_books2 + 1
            marks_books2 = marks_books2 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['# Books'] == 3:
            count_books3 = count_books3 + 1
            marks_books3 = marks_books3 + dataframe.iloc[i]['Maths %']
        if dataframe.iloc[i]['# Books'] == 4:
            count_books4 = count_books4 + 1
            marks_books4 = marks_books4 + dataframe.iloc[i]['Maths %']
print('Average % of marks for Books(No Books) = ',
      (marks_books1 / count_books1))
print('Average % of marks for Books(1-10) = ',
      (marks_books2 / count_books2))
print('Average % of marks for Books(11-25) = ',
      (marks_books3 / count_books3))
print('Average % of marks for Books(25+) = ',
      (marks_books4 / count_books4)) '''
