import math
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
dataframe = pd.read_csv('../../Dataset/nas-pupil-marks.csv')
count_boys = np.full(33, 0)
count_girls = np.full(33, 0)
marks_boys = np.full(33, 0.00)
marks_girls = np.full(33, 0.00)
states = ['AN', 'AP', 'AR', 'BR', 'CG', 'CH', 'DD', 'DL', 'DN',
          'GA', 'GJ', 'HP', 'HR', 'JH', 'JK', 'KA', 'KL', 'MG', 'MH', 'MN',
          'MP', 'MZ', 'NG', 'OR', 'PB', 'PY', 'RJ', 'SK', 'TN',
          'TR', 'UK', 'UP', 'WB']
for i in range(0, len(dataframe)):
    if (math.isnan(dataframe.iloc[i]['Maths %'])) is False:
            state_code = states.index(dataframe.iloc[i]['State'])
            if dataframe.iloc[i]['Gender'] == 1:
                count_boys[state_code] = count_boys[state_code] + 1
                marks_boys[state_code] = marks_boys[state_code] + dataframe.iloc[i]['Maths %']
            if dataframe.iloc[i]['Gender'] == 2:
                count_girls[state_code] = count_girls[state_code] + 1
                marks_girls[state_code] = marks_girls[state_code] + dataframe.iloc[i]['Maths %']
boys_starrer = 0
girls_starrer = 0
same_starrer = 0
max_diff = 0.00
state_max_diff = ''
for i in range(0, len(states)):
    avg_marks_boys = marks_boys[i] / count_boys[i]
    avg_marks_girls = marks_girls[i] / count_girls[i]
    print('Average % marks for boys in ', states[i], 'is ', avg_marks_boys)
    print('Average % marks for girls in ', states[i], 'is ', avg_marks_girls)
    if abs(avg_marks_boys - avg_marks_girls) > max_diff:
        max_diff = abs(avg_marks_boys - avg_marks_girls)
        state_max_diff = states[i]
    if avg_marks_boys > avg_marks_girls:
        boys_starrer = boys_starrer + 1
    elif avg_marks_boys < avg_marks_girls:
        girls_starrer = girls_starrer + 1
    else:
        girls_starrer = girls_starrer + 1
print('Number of states where boys tops ', boys_starrer)
print('Number of states where girls tops ', girls_starrer)
print('The max differece in the performance is found to be: ', max_diff, ' in state', state_max_diff)
'''